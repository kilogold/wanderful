﻿using UnityEngine;
using System.Collections;

public class ClientLogic : MonoBehaviour 
{
	public GameObject ownerPrefab;
	public GameObject proxyPrefab;
	public GameObject serverPrefab;
	public string ServerIP;

	// Use this for initialization
	void Start () 
	{
		Debug.Log ("connecting to server");
		uLink.Network.Connect( ServerIP, 9999 );
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void uLink_OnConnectedToServer(  )
	{
		Debug.Log ("player connected");
		uLink.Network.Instantiate(
								uLink.Network.player,
								proxyPrefab, 
		                          ownerPrefab, 
		                          serverPrefab, 
		                          new Vector3(0,3,0), 
		                          Quaternion.identity,0);
	}
        
}
