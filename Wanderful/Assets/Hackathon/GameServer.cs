﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

public class GameServer : MonoBehaviour 
{
	/// <summary>
	/// The num players.
	/// </summary>
	public int numPlayers = 2;
	
	/// <summary>
	/// The game server port.
	/// </summary>
	public int gameServerPort = 9999;
	
	/// <summary>
	/// The match start time stamp.
	/// </summary>
	public DateTime matchStartTimeStamp;
	
	/// <summary>
	/// The match end time stamp.
	/// </summary>
	public DateTime matchEndTimeStamp;

	/******************************
	 *		UNITY API METHODS
	 ******************************/
	
	/// <summary>
	/// Awake initialization.
	/// </summary>
	void Awake()
	{
	}
	
	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () 
	{
		uLink.Network.InitializeServer(numPlayers, gameServerPort );
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
	}
	
	
	/***************************************
	 *		ULINK/ULOBBY API METHODS
	 ***************************************/
	
	
	/// <summary>
	/// uLink callback when server initialized.
	/// </summary>
	[SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed.")]
	private void uLink_OnServerInitialized()
	{
		Debug.Log("(ULink) Server Initialized!");
		// Now we connect to uLobby.
		Debug.Log ("(ULink) Connecting to ULobby");
	}
	
	/// <summary>
	/// uLink callback for player disconnected.
	/// </summary>
	/// <param name="disconnectedPlayer">Disconnected player.</param>
	[SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed.")]
	private void uLink_OnPlayerDisconnected( uLink.NetworkPlayer disconnectedPlayer )
	{
		Debug.Log ("(ULink) Player Disconnected: " + disconnectedPlayer.endpoint.Address.ToString());
		uLink.Network.DestroyPlayerObjects( disconnectedPlayer );
		uLink.Network.RemoveRPCs( disconnectedPlayer );
	}
	
	/// <summary>
	/// Callback for when server is uninitialized.
	/// </summary>
	[SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed.")]
	private void uLink_OnServerUninitialized()
	{
	}
	
	/// <summary>
	/// uLink callback for player connected.
	/// </summary>
	/// <param name="connectingPlayer">Connecting player.</param>
	[SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed.")]
	private void uLink_OnPlayerConnected( uLink.NetworkPlayer connectingPlayer )
	{
		// We have a player connected now. 
		Debug.Log ("(ULink) Player Connected: " + connectingPlayer.endpoint.Address.ToString());
	}
}