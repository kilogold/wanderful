﻿using UnityEngine;
using System.Collections;

public class EndingMessage : MonoBehaviour {

	//Ending text to modify
	public TextMesh messageText;

	public float typingDelay;
	private float currentTypingDelay; 

	public string messageToDisplay;
	public int currentMessageStringIndex;

	//The floor that will disable to reveal the ending shaft fall.
	public GameObject trapFloor;

	// Use this for initialization
	void Start () 
	{
		//Clear the message
		messageText.text = string.Empty;
	}
	
	// Update is called once per frame
	void Update () 
	{
		currentTypingDelay -= Time.deltaTime;

		//If the typing timer has finished
		//AND
		//We have more letters to process...
		if( currentTypingDelay <= 0 && currentMessageStringIndex < messageToDisplay.Length )
		{
			//reset the timer
			currentTypingDelay = typingDelay;

			//Advance the text
			messageText.text += messageToDisplay[currentMessageStringIndex];

			//Increment index to the next string character
			currentMessageStringIndex++;


			//Only if we're finished with the message...
			if( currentMessageStringIndex >= messageToDisplay.Length )
			{
				//Finalize if we've reached the final character.
				//We'll call this in a little bit. Let's give the player a bit of time to ponder...
				Invoke( "FinishEndingMessage", 3 );
			}
		}
	}

	void FinishEndingMessage()
	{
		//We disable the trap door.
		//The player will begin the descent to the ending.
		trapFloor.SetActive( false );

	}
}
