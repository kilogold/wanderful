﻿using UnityEngine;
using System.Collections;

public class BoardBlendTexture : MonoBehaviour {

	//Begin and end thresholds of the blending.
	//The beginning marks when the painting starts to blend.
	//The end marks when the painting is finally fully transformed.
	public float transformBeginThresholdRadius;
	public float transformEndThresholdRadius;

	//Player reference to know where the player is in order to query distance.
	public GameObject playerReference;

	//The text material to fade out
	public MeshRenderer boardTextRenderer;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 boardToPlayerVector = playerReference.transform.position - transform.position;
		float currentDistanceRadius = boardToPlayerVector.magnitude;


			//Let's see how far deep we are within the threshold
			float penetratingRadius = transformBeginThresholdRadius - currentDistanceRadius;

			//Grab the percentage of penetration
			float penetrationPercentage = penetratingRadius / transformBeginThresholdRadius;

			penetrationPercentage = Mathf.Clamp(penetrationPercentage,0.0f,1.0f);

			//Set the blend value for the board
			this.renderer.material.SetFloat("_Blend", penetrationPercentage);

			//Set blend value for the text (ONLY IF WE HAVE IT)
			if( null != boardTextRenderer )
			{
				boardTextRenderer.material.SetColor("_Color", new Color(0,0,0, penetrationPercentage * 2 ));
			}
	}
}
