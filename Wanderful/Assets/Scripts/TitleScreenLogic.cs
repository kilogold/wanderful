﻿using UnityEngine;
using System.Collections;

public class TitleScreenLogic : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		const float speed = 10; 
		transform.Rotate(0,speed * Time.deltaTime, 0);
	
		if( Input.anyKey )
		{
			Application.LoadLevel("MainScene_KelvinOffsetWork");
		}
	}
}
