﻿using UnityEngine;
using System.Collections;

public class EndingMessageTrigger : MonoBehaviour {

	//The target to activate from the trigger.
	public EndingMessage textScriptTarget;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter( Collider collider ) 
	{
		//We are ready to see the game ending.
		//The player is in place.

		//We'll disable the controls

		//We'll start the final text script
		textScriptTarget.enabled = true;
	}
}
