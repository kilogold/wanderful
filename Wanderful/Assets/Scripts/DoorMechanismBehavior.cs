﻿using UnityEngine;
using System.Collections;

public class DoorMechanismBehavior : MonoBehaviour {

	//By default the door is open.
	bool isClosing;
	public float destinationPositionHeight;
	public float originPositionHeight;
	public float translationSpeed;

	// Use this for initialization
	void Start () {
		isClosing = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (true == isClosing) 
		{
			if( transform.position.y < destinationPositionHeight )
			{
				//Trigger moving the doors to close.
				transform.Translate(0,translationSpeed * Time.deltaTime,0);
				
				//If we reached the "closed" version of the doors....
				if( transform.position.y >= destinationPositionHeight )
				{
					//Set position to "closed" doors.
					//This should stop it from executing this far into the logic again.
					transform.position = new Vector3(
						transform.position.x,
						destinationPositionHeight,
						transform.position.z);
				}
			}
		}
	}

	void Closer()
	{
		Debug.Log("Closing Doors for Ending.");
		isClosing = true;
	}
}
