﻿using UnityEngine;
using System.Collections;

public class PortalTeleporterScript : MonoBehaviour {

	public GameObject destinationTransform;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerEnter( Collider collider ) 
	{
		//Offset so we land a little bit past the target.
		Vector3 destinationOffset = destinationTransform.transform.forward * 2.0f;

		collider.transform.position = destinationTransform.transform.position + destinationOffset;
	}
}
